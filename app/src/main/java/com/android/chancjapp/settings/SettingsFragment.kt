package com.android.chancjapp.settings

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.android.chancjapp.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }
}