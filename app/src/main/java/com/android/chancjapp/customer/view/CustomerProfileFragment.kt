package com.android.chancjapp.customer.view

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.android.chancjapp.R
import com.android.chancjapp.dashboard.DashboardActivity
import com.android.chancjapp.databinding.FragmentCustomerProfileBinding

class CustomerProfileFragment : Fragment() {

    private lateinit var binding: FragmentCustomerProfileBinding
    private val args: CustomerProfileFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_customer_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        binding.apply {
            setDisplayGreetings()
            setupListeners()
        }
    }

    private fun FragmentCustomerProfileBinding.setupListeners() {
        with(btnOk) {
            setOnClickListener {
                val action =
                    CustomerProfileFragmentDirections.actionCustomerProfileFragmentToCustomersFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun FragmentCustomerProfileBinding.setDisplayGreetings() {
        tvWelcomeGreetings.text = when {
            !TextUtils.isEmpty(args.lastname) && !TextUtils.isEmpty(args.firstname) -> String.format(
                "Welcome! %s, %s",
                args.lastname,
                args.firstname
            )
            else -> String.format("Welcome! %s", args.firstname)
        }

    }
}